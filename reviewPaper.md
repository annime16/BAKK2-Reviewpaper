1. What is a publishable paper?
    - Does it make a sufficient contribution?
 The paper provides an overview of IPv6 addressing strategies for IoT. It presents facets of IPv6 for IoT node addressing as well as possible
 network topologies for realistic deployment. 

2. What is the purpose of the paper?
The paper aims to highlight IPv6 addressing schemes for nodes and gateways in various topologies.
It emphasizes issues like:
  - Node Miniaturisation
  - Renumbering Challenges
  - Multihoming Challenges
  - Proxying and Tracking non-IP nodes

3. Is the paper appropriate?
It is in the field of computer science, and it provides a general overview but is at the same time quite technical.

4. Is the goal of this paper significant?
The paper provides a good overview of the term IoT and IPv6 as well as applicable network topologies.

5. Is the method of approach valid?
The different approaches for realistic uses in network topologies are explained very well and structured. 
Each subsection refers to a part of a figure, e.g subsection A. belongs to a) in Figure 2. 
This provides a good mapping to the use cases and the reader can compare the approaches visually which helps a lot.

6. Is the actual execution of the research correct?
The purpose and goal of each section described in the introduction are presented and carried out correctly and accordingly.

7. Are the correct conclusions being drawn from the results?
The conclusion of the paper is that certain features provided by IPv6 in IoT environments are not applicable for different reasons.
It is a very good conclusion that sums up the main points that have to be considered when actually impelementing their discussed solutions.
There is a short recommendation for future implementations as well.
The conclusion is comprehensible since the sections' content lead to it, step by step.


8. Is the presentation satisfactory?
The presentation is satisfactory overall. However, there are some sentences which should be revisioned. Not because, the message is not conveyed but
rather because there are missing adverbs, or small grammatical mistakes. These are only slight mistakes that can be overlooked since they do not occur often 
throughout the paper.
The paper in itself is well written and understandable.

9. What did you learn by reading the paper?
I got a pretty good overview of what IoT is. The paper gave a pretty good explanation to the background of IPv6 and its facets for IoT node addressing.
Directly following that, the various topologies for IoT deployment were elaborated using a figure and providing explanations.
In a separate section the mobility implications for IoT were explained and how well these might work if actually implemented.
There are also several solutions to allocate IPv6 addresses for IoT nodes. These also include a short elaboration on disadvantages that come with the explained solution.

