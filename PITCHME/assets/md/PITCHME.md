# IPv6 Addressing Strategies for IoT
### by Teemu Savolainen et al 
---

### Does it make a sufficient contribution?
- Overview of IPv6 addressing strategies
- IPv6 for IoT node addressing
- Network topologies for deployment

Note: 
What is a publishable paper?

The paper provides an overview of IPv6 addressing strategies for IoT. It presents facets of IPv6 for IoT node addressing as well as possible
 network topologies for realistic deployment. 
---

### What is the purpose of the paper?

  - Node Miniaturisation
  - Renumbering Challenges
  - Multihoming Challenges
  - Proxying and Tracking non-IP nodes

Note:
The paper aims to highlight IPv6 addressing schemes for nodes and gateways in various topologies.
It emphasizes issues like:
  - Node Miniaturisation
  - Renumbering Challenges
  - Multihoming Challenges
  - Proxying and Tracking non-IP nodes
---
### Is the paper appropriate?
- General Overview
- Technical

Note:
It is in the field of computer science, and it provides a general overview but is at the same time quite technical.
---
### Is the goal of this paper significant?
- Overview
- Applicable Network Topologies

But: Theoretical!

Note:
The paper provides a good overview of the term IoT and IPv6 as well as applicable network topologies.
---
### Is the method of approach valid?

- realistic ideas
- mapping of figure to explanation 
- very general
- for purpose of giving an overview approach sufficient

Note:
The different approaches for realistic uses in network topologies are explained very well and structured. 
Each subsection refers to a part of a figure, e.g subsection A. belongs to a) in Figure 2. 
This provides a good mapping to the use cases and the reader can compare the approaches visually which helps a lot.
---
### Is the actual execution of the research correct?

 
Note:
The purpose and goal of each section described in the introduction are presented and carried out correctly and accordingly.
What authors announced they would do in each section, they DID. 
Not much details about the actual experiment since it is an overview

---
### Are the correct conclusions being drawn from the results?

- some things do not work for certain IoT nodes
- main points of paper listed again
- recommendations for the future

Note:
The conclusion of the paper is that certain features provided by IPv6 in IoT environments are not applicable for different reasons.
It is a very good conclusion that sums up the main points that have to be considered when actually impelementing their discussed solutions.
There is a short recommendation for future implementations as well.
The conclusion is comprehensible since the sections' content lead to it, step by step.
---

### Is the presentation satisfactory?

- abstract describes the paper
- introduction elaborates sections
- figures legible
- grammar

Note:
The presentation is satisfactory overall. However, there are some sentences which should be revisioned. Not because, the message is not conveyed but
rather because there are missing adverbs, or small grammatical mistakes. These are only slight mistakes that can be overlooked since they do not occur often 
throughout the paper.
The paper in itself is well written and understandable.
It could have been longer.
Experiment could be explained in more detail.

---
### What did I learn?

Note:
I got a pretty good overview of what IoT is. The paper gave a pretty good explanation to the background of IPv6 and its facets for IoT node addressing.
Directly following that, the various topologies for IoT deployment were elaborated using a figure and providing explanations.
In a separate section the mobility implications for IoT were explained and how well these might work if actually implemented.
There are also several solutions to allocate IPv6 addresses for IoT nodes. These also include a short elaboration on disadvantages that come with the explained solution.
---
# Thank you!